# TOD
Today, To date, To do.

### Overview  

This software is a Java implementation of a personal manager.
This program is a free software, you can redistribute and/or modify it. It was developed for the course of Object-Oriented Programming, faculty of Engineering and Computer Science, University of Bologna, Italy.  

### Usage instructions  
1. Download the jar file.  
2. Run it.  

### Developers  
- Martina Cavallucci - View  
- Alfredo Tonelli - Model
- Claudia Severi - Controller